const processData = require('./src/es')
const R = require('ramda')
export const logSearch = async (event, context, callback) => {
  try {
    let data = await processData()
    if (data.hasErrors) {
      console.log(data.result)
      callback(new Error(data.result), null)
    } else {
      console.log(data.result)
      callback(null, data.result)
    }
   // R.ifElse(data.hasErrors, callback(new Error(data.result), null), callback(null, data.result))
  } catch (error) {
    console.log(error)
    callback(error, null)
  }
}
