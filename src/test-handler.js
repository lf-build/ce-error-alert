
const processData = require('./es')

const getData = async () => {
  try {
    let data = await processData()
    console.log(data.result)
  } catch (error) {
    console.log(error)
  }
}

getData()


// var logSearch = require('../handler')
// var event = {
//   'account': '123456789012',
//   'region': 'us-east-1',
//   'detail': {},
//   'detail-type': 'Scheduled Event',
//   'source': 'aws.events',
//   'time': '1970-01-01T00:00:00Z',
//   'id': 'cdc73f9d-aea9-11e3-9d5a-835b769c0d9c',
//   'resources': [
//     'arn:aws:events:us-east-1:123456789012:rule/my-schedule'
//   ]
// }

// var context = {
//   'succeed': function (data) {
//     console.log(JSON.stringify(data, null, '\t'))
//   },
//   'fail': function (err) {
//     console.log('context.fail occurred')
//     console.log(JSON.stringify(err, null, '\t'))
//   }

// }

// function callback (error, data) {
//   if (error) {
//     console.log('error: ' + error)
//   } else {
//     console.log(data)
//   }
// }
// //MyLambdaFunction['logSearch'](event, context, callback)
// logSearch(event, context, callback)
