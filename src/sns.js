const AWS = require('aws-sdk')
AWS.config.region = 'ap-southeast-1'

const sendEmail = (emailBody) => {
  let sns = new AWS.SNS()
  var params = {
    Message: emailBody,
    Subject: 'CE Application alert',
    TopicArn: 'arn:aws:sns:ap-southeast-1:993978665313:sns-qbera-prod-alarm'
  }
  return new Promise((resolve, reject) => {
    sns.publish(params, (err, data) => {
      if (err) {
        return reject(err)
      }
      return resolve('success')
    })
  })
}

module.exports = sendEmail
