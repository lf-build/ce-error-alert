const ElasticSearch = require('elasticsearch')

const moment = require('moment')

const R = require('ramda')
const sendEmail = require('./sns')

const esClient = new ElasticSearch.Client({

  host: 'https://elastic:boutmFYFG1SRTmyDeChR51gj@367cf754870339416a86b5c37e93ac03.ap-southeast-1.aws.found.io:9243'

})

const beforeOneHourTimeStamp = () => {
  let ts = moment().utc().subtract(1, 'h').unix()

  let ms = `${ts}000`

  return new Promise((resolve, reject) => {
    resolve(ms)
  })
}

const peformSearchinElastic = async(epochTime) => {
  try {
    let results = await esClient.search({

      index: 'qbera-prod-*',

      body: {
        'size': 25,

        '_source': ['container_name', 'log', '@timestamp'],

        'query': {

          'bool': {

            'must': [{

              'match': {

                'log': 'timeout Unreachable socket exception'

              }

            },

            {

              'range': {

                '@timestamp': {

                  'gte': epochTime,

                  'lte': 'now'

                }

              }

            }

            ]

          }

        }

      }

    })

    if (results.hits.hits) {
      return results.hits.hits
    }

    return null
  } catch (error) {
    console.log(error)
  }
}

const sendAlert = async (data) => {
  if (data.length > 0) {
    let filterData = data.map(d => {
      return R.prop('_source', d)
    })
    var str = 'Following Errors occured'

    filterData.forEach(d => {
      for (let [key, value] of Object.entries(d)) {
        let template = `
                      ${key} : ${value}
                      `
        str = str.concat(template)
      }
    })
    let emailResult = await sendEmail(str)
    if (emailResult !== 'success') {
      return {
        result: 'Error occured in sending email',
        hasErrors: true
      }
    } else {
      return {
        result: 'Email sent successfully',
        hasErrors: false
      }
    }
  }
  return {
    result: 'No matching data found in logs',
    hasErrors: false
  }
}

const processData = R.composeP(sendAlert, peformSearchinElastic, beforeOneHourTimeStamp)

module.exports = processData
